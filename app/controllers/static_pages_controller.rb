require 'tesseract'

class StaticPagesController < ApplicationController

	def home

		e = Tesseract::Engine.new {|e|
		  e.language  = :eng
		  e.blacklist = '|'
		}

		@result = e.text_for(Rails.root.join('public', 'ss.png')).strip
	end
end
